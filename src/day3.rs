pub fn common_bits<const N: usize>(v: &[String]) -> [u8; N] {
    let mut count_ones: [u16; N] = [0; N];
    let mut count_zero: [u16; N] = [0; N];

    let v_iter = v.iter();

    for binary in v_iter {
        for (i, c) in binary.chars().enumerate() {
            if c == '1' {
                count_ones[i] += 1;
            } else {
                count_zero[i] += 1;
            }
        }
    }
    let mut out: [u8; N] = [0; N];
    for i in 0..N {
        if count_ones[i] > count_zero[i] {
            out[i] = 1;
        }
    }
    out
}

fn compare_char_and_u8_until<const N: usize>(lhs: &[char; N], rhs: &[u8], i: usize) -> bool {
    if i == 0 {
        return true;
    }
    for (p, pcb) in lhs.iter().take(i).enumerate() {
        let prev_bit = rhs[p] as char;
        if prev_bit != *pcb {
            return false;
        }
    }
    true
}

pub fn common_bits_by_elimination<const N: usize>(
    v: &[String],
    pred: &dyn Fn(i32, i32) -> bool,
) -> [u8; N] {
    let mut out: [u8; N] = [0; N];
    let mut prev_common_bits: [char; N] = ['0'; N];
    // Loop each bit
    for (i, bit_out) in out.iter_mut().enumerate() {
        let mut count_ones = 0;
        let mut count_zero = 0;

        let mut first_match: Option<String> = None;
        for binary in v.iter() {
            if compare_char_and_u8_until(&prev_common_bits, binary.as_bytes(), i) {
                let bit = binary.as_bytes()[i] as char;
                if bit == '1' {
                    count_ones += 1;
                } else {
                    count_zero += 1;
                }
                if first_match.is_none() {
                    first_match = Some(binary.to_string());
                }
            }
        }

        if count_ones + count_zero == 1 {
            for (i, c) in first_match
                .expect("Didn't find unique string!")
                .chars()
                .enumerate()
            {
                if c == '1' {
                    out[i] = 1;
                } else {
                    out[i] = 0;
                }
            }
            return out;
        }

        if pred(count_ones, count_zero) {
            *bit_out = 1u8;
            prev_common_bits[i] = '1';
        }
    }
    out
}

pub fn multiply_by_bitinv<const N: usize>(bits: [u8; N]) -> usize {
    let mut lhs = 0;
    let mut rhs = 0;
    for i in 0..N {
        if bits[N - i - 1] == 1 {
            lhs += 2usize.pow(i.try_into().unwrap());
        } else {
            rhs += 2usize.pow(i.try_into().unwrap());
        }
    }

    lhs * rhs
}

pub fn binary_to_base10<const N: usize>(bits: [u8; N]) -> usize {
    let mut out = 0;
    for i in 0..N {
        if bits[N - i - 1] == 1 {
            out += 2usize.pow(i.try_into().unwrap());
        }
    }
    out
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_common_bits() {
        let input = vec![
            String::from("00100"),
            String::from("11110"),
            String::from("10110"),
            String::from("10111"),
            String::from("10101"),
            String::from("01111"),
            String::from("00111"),
            String::from("11100"),
            String::from("10000"),
            String::from("11001"),
            String::from("00010"),
            String::from("01010"),
        ];
        let result = common_bits::<5>(&input);
        assert_eq!(multiply_by_bitinv(result), 198);
    }

    #[test]
    fn test_common_bits_from_file() {
        let filelines = read_filelines("./data/day3.txt").expect("Failed reading lines, day3");
        let input = filelines_to_vector::<String>(filelines);
        let result = common_bits::<12>(&input);
        assert_eq!(multiply_by_bitinv(result), 3847100);
    }

    #[test]
    fn test_common_bits_by_elimination() {
        let input = vec![
            String::from("00100"),
            String::from("11110"),
            String::from("10110"),
            String::from("10111"),
            String::from("10101"),
            String::from("01111"),
            String::from("00111"),
            String::from("11100"),
            String::from("10000"),
            String::from("11001"),
            String::from("00010"),
            String::from("01010"),
        ];
        // lsr = O2_gen_rating * CO2_scrub_rating
        // O2_gen_rating = most common  (keep 1 on equality)
        let gte = |a, b| a >= b;
        let o2_gen_rating = common_bits_by_elimination::<5>(&input, &gte);
        // CO2_scrub_rating = least common (keep 0 on equality)
        let lte = |a, b| a < b;
        let co2_scrub_rating = common_bits_by_elimination::<5>(&input, &lte);
        let result = binary_to_base10(o2_gen_rating) * binary_to_base10(co2_scrub_rating);
        assert_eq!(result, 230);
    }

    #[test]
    fn test_common_bits_by_elimination_from_file() {
        let filelines = read_filelines("./data/day3.txt").expect("Failed reading lines, day3");
        let input = filelines_to_vector::<String>(filelines);
        // lsr = O2_gen_rating * CO2_scrub_rating
        // O2_gen_rating = most common  (keep 1 on equality)
        let gte = |a, b| a >= b;
        let o2_gen_rating = common_bits_by_elimination::<12>(&input, &gte);
        // CO2_scrub_rating = least common (keep 0 on equality)
        let lte = |a, b| a < b;
        let co2_scrub_rating = common_bits_by_elimination::<12>(&input, &lte);
        let result = binary_to_base10(o2_gen_rating) * binary_to_base10(co2_scrub_rating);
        assert_eq!(result, 4105235);
    }
}
