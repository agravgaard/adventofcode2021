pub fn move_manhattan(v: &[String]) -> i32 {
    let mut depth = 0;
    let mut horizontal_pos = 0;

    let v_iter = v.iter();

    for element in v_iter {
        let (direction, value_str) = element.split_once(' ').expect("Wrong input format");
        let value = value_str.parse::<i32>().expect("Input was not a number");
        if direction == "forward" {
            horizontal_pos += value;
        } else if direction == "up" {
            depth -= value;
        } else if direction == "down" {
            depth += value;
        }
    }
    depth * horizontal_pos
}

pub fn move_by_aim(v: &[String]) -> i32 {
    let mut depth = 0;
    let mut horizontal_pos = 0;
    let mut aim = 0;

    let v_iter = v.iter();

    for element in v_iter {
        let (direction, value_str) = element.split_once(' ').expect("Wrong input format");
        let value = value_str.parse::<i32>().expect("Input was not a number");
        if direction == "forward" {
            horizontal_pos += value;
            depth += aim * value;
        } else if direction == "up" {
            aim -= value;
        } else if direction == "down" {
            aim += value;
        }
    }
    depth * horizontal_pos
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_move_manhattan() {
        let input = vec![
            String::from("forward 5"),
            String::from("down 5"),
            String::from("forward 8"),
            String::from("up 3"),
            String::from("down 8"),
            String::from("forward 2"),
        ];
        let result = move_manhattan(&input);
        assert_eq!(result, 150);
    }

    #[test]
    fn test_move_manhattan_from_file() {
        let filelines = read_filelines("./data/day2.txt").expect("Failed reading lines, day2");
        let input = filelines_to_vector::<String>(filelines);
        let result = move_manhattan(&input);
        assert_eq!(result, 2070300);
    }

    #[test]
    fn test_move_by_aim() {
        let input = vec![
            String::from("forward 5"),
            String::from("down 5"),
            String::from("forward 8"),
            String::from("up 3"),
            String::from("down 8"),
            String::from("forward 2"),
        ];
        let result = move_by_aim(&input);
        assert_eq!(result, 900);
    }

    #[test]
    fn test_move_by_aim_from_file() {
        let filelines = read_filelines("./data/day2.txt").expect("Failed reading lines, day2");
        let input = filelines_to_vector::<String>(filelines);
        let result = move_by_aim(&input);
        assert_eq!(result, 2078985210);
    }
}
