pub fn count_increases(v: &[usize]) -> usize {
    let mut count = 0;

    let v_iter = v.iter();
    let mut prev_val = usize::MAX;

    for val in v_iter {
        if val > &prev_val {
            count += 1;
        }
        prev_val = *val;
    }
    count
}

pub fn count_increases_3sum(v: &[usize]) -> usize {
    let mut count = 0;

    let mut prev_v_iter = v.iter();
    let mut v_iter = v.iter();
    v_iter.next();
    v_iter.next();
    v_iter.next();

    for val in v_iter {
        if val > prev_v_iter.next().unwrap() {
            count += 1;
        }
    }
    count
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::filereader::{filelines_to_vector, read_filelines};

    #[test]
    fn test_count_increases() {
        let input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        let result = count_increases(&input);
        assert_eq!(result, 7);
    }

    #[test]
    fn test_count_increases_from_file() {
        let filelines = read_filelines("./data/day1.txt").expect("Failed reading lines, day1");
        let input = filelines_to_vector(filelines);
        let result = count_increases(&input);
        assert_eq!(result, 1752);
    }

    #[test]
    fn test_count_increases_3sum() {
        let input = [199, 200, 208, 210, 200, 207, 240, 269, 260, 263];
        let result = count_increases_3sum(&input);
        assert_eq!(result, 5);
    }

    #[test]
    fn test_count_increases_3sum_from_file() {
        let filelines = read_filelines("./data/day1.txt").expect("Failed reading lines, day1");
        let input = filelines_to_vector(filelines);
        let result = count_increases_3sum(&input);
        assert_eq!(result, 1781);
    }
}
